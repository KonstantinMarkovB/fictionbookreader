package tok.description;

import org.junit.Test;
import tok.AbstractTest;
import tok.fictionbook.model.type.TitleInfoType;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static tok.fictionbook.model.type.TitleInfoType.*;

public class TitleInfoTest extends AbstractTest {
    private static final String fileName = "/title-test.fb2";

    private final TitleInfoType titleInfo = read().getDescription().getTitleInfo();

    @Test
    public void testAuthor() {
        List<Author> expected = buildTestAuthor();
        List<Author> actual = titleInfo.getAuthor();
        assertListsEquals(expected, actual);
    }

    private List<Author> buildTestAuthor() {
        return listOf(Author.builder()
                        .firstName("Eric")
                        .lastName("Weiner")
                        .middleName("Sad")
                        .build(),
                Author.builder()
                        .nickname("Eric n n")
                        .build());
    }

    @Test
    public void bookTitle() {
        String expected = "The Geography of Bliss: One Grump's Search for the Happiest Places in the World";
        String actual = titleInfo.getBookTitle().getValue();
        assertEquals(expected, actual);
    }

    @Test
    public void coverpage(){
        String expected = "#_0.jpg";
        String actual = titleInfo.getCoverpage().getImage().get(0).getHref();
        assertEquals(expected, actual);
    }

    @Override
    protected String getFileName() {
        return fileName;
    }
}
