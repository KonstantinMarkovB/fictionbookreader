package tok.body;

import org.junit.Test;
import tok.AbstractTest;
import tok.fictionbook.model.type.ImageType;

import static org.junit.Assert.assertEquals;

public class ImageTest extends AbstractTest {
    private static final String fileName = "/body-test.fb2";

    private final ImageType imageType = read().getBody().getImage();

    @Test
    public void href() {
        String expected = "#picture.jpg";
        String actual = imageType.getHref();
        assertEquals(expected, actual);
    }

    @Test
    public void title() {
        String expected = "tttt";
        String actual = imageType.getTitle();
        assertEquals(expected, actual);
    }

    @Test
    public void alt() {
        String expected = "alt";
        String actual = imageType.getAlt();
        assertEquals(expected, actual);
    }

    @Override
    protected String getFileName() {
        return fileName;
    }
}
