package tok.body;

import org.junit.Test;
import tok.AbstractTest;
import tok.fictionbook.model.element.*;
import tok.fictionbook.model.type.ImageType;
import tok.fictionbook.model.type.PType;
import tok.fictionbook.model.type.TitleType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TitleTest extends AbstractTest {
    private static final String fileName = "/body-test.fb2";

    private final TitleType titleType = read().getBody().getTitle();

    @Test
    public void lang() {
        String expected = "en";
        String actual = titleType.getLang();
        assertEquals(expected, actual);
    }

    @Test
    public void pTag() {
        Object element1 = titleType.getPOrEmptyLine().get(0);
        Object element2 = titleType.getPOrEmptyLine().get(1);
        Object element3 = titleType.getPOrEmptyLine().get(2);

        assertSimplePElement(element1);
        assertEmptyLine(element2);

        assertComplexPElement(element3);
    }

    private void assertSimplePElement(Object object) {
        assertTrue(object instanceof PType);
        assertEquals(1, ((PType) object).getContent().size());
        assertTrue(((PType) object).getContent().get(0) instanceof String);
        assertEquals("p tag", ((PType) object).getContent().get(0));
    }

    private void assertEmptyLine(Object object) {
        assertTrue(object instanceof EmptyLine);
    }

    private void assertComplexPElement(Object object) {
        assertTrue(object instanceof PType);
        PType p = (PType) object;

        int i = 0;

        assertEquals("vvv", ((Strong) p.getContent().get(i++)).getContent().get(0));

        assertEquals("bbb", ((Emphasis) p.getContent().get(i++)).getContent().get(0));

        assertEquals("fff", ((Style) p.getContent().get(i++)).getName());

        assertEquals("p2 tg", p.getContent().get(i++));

        assertEquals("sdfdsf", ((A) p.getContent().get(i++)).getHref());

        assertEquals("fgh", ((Strikethrough) p.getContent().get(i++)).getContent().get(0));

        assertEquals("sub", ((Sub) p.getContent().get(i++)).getContent().get(0));

        assertEquals("sup", ((Sup) p.getContent().get(i++)).getContent().get(0));

        assertEquals("codejava", ((Code) p.getContent().get(i++)).getContent().get(0));

        assertEquals("sdfd", ((ImageType) p.getContent().get(i++)).getHref());
    }

    @Override
    protected String getFileName() {
        return fileName;
    }
}
