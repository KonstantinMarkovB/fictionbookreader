package tok.body;

import org.junit.Test;
import tok.AbstractTest;
import tok.fictionbook.model.type.EpigraphType;
import tok.fictionbook.model.type.PType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EpigraphTest extends AbstractTest {
    private static final String fileName = "/body-test.fb2";

    private final EpigraphType epigraph = read().getBody().getEpigraph().get(0);

    @Test
    public void pTag() {
        Object element = epigraph.getPOrPoemOrCite().get(0);
        assertSimplePElement(element, "dsfsd");
    }

    private void assertSimplePElement(Object object, String text) {
        assertTrue(object instanceof PType);
        assertEquals(1, ((PType) object).getContent().size());
        assertTrue(((PType) object).getContent().get(0) instanceof String);
        assertEquals(text, ((PType) object).getContent().get(0));
    }

    @Test
    public void testTextAuthor() {
        PType p = epigraph.getTextAuthor().get(0);
        assertEquals("dsf", p.getContent().get(0));
    }

    @Override
    protected String getFileName() {
        return fileName;
    }
}
