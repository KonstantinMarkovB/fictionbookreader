package tok;

import org.junit.Assert;
import tok.fictionbook.FictionBookReader;
import tok.fictionbook.model.FictionBook;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public abstract class AbstractTest {
    protected FictionBook read() {
        try {
            return new FictionBookReader().read(getFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        throw new RuntimeException();
    }

    protected abstract String getFileName();

    private File getFile() {
        try {
            return new File(this.getClass().getResource(getFileName()).toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    protected <T> List<T> listOf(T... args) {
        return Arrays.asList(args);
    }

    protected <T> void assertListsEquals(List<T> expected, List<T> actual) {
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
}