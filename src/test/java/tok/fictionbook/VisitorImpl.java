package tok.fictionbook;

import tok.fictionbook.model.FictionBook;
import tok.fictionbook.model.Visitor;
import tok.fictionbook.model.element.SubTitle;
import tok.fictionbook.model.table.TableType;
import tok.fictionbook.model.table.Td;
import tok.fictionbook.model.table.Th;
import tok.fictionbook.model.type.*;

public class VisitorImpl implements Visitor {

    @Override
    public void startOf(FictionBook o) {
        System.out.println("<fb>");
    }

    @Override
    public void endOf(FictionBook o) {
        System.out.println("</fb>");
    }

    @Override
    public void startOf(BodyType o) {
        System.out.println("<body>");
    }

    @Override
    public void endOf(BodyType o) {
        System.out.println("</body>");

    }

    @Override
    public void startOf(NotesBodyType o) {

    }

    @Override
    public void endOf(NotesBodyType o) {

    }

    @Override
    public void startOf(EpigraphType o) {

    }

    @Override
    public void endOf(EpigraphType o) {

    }

    @Override
    public void startOf(SectionType o) {
        System.out.println("<section>");

    }

    @Override
    public void endOf(SectionType o) {
        System.out.println("</section>");

    }

    @Override
    public void startOf(TableType o) {

    }

    @Override
    public void endOf(TableType o) {

    }

    @Override
    public void startOf(TitleType o) {
        System.out.println("<title>");

    }

    @Override
    public void endOf(TitleType o) {
        System.out.println("</title>");

    }

    @Override
    public void startOf(TableType.Tr o) {

    }

    @Override
    public void endOf(TableType.Tr o) {

    }

    @Override
    public void startOf(Th o) {

    }

    @Override
    public void endOf(Th o) {

    }

    @Override
    public void startOf(Td o) {

    }

    @Override
    public void endOf(Td o) {

    }

    @Override
    public void startOf(AnnotationType o) {

    }

    @Override
    public void endOf(AnnotationType o) {

    }

    @Override
    public void startOf(PoemType o) {

    }

    @Override
    public void endOf(PoemType o) {

    }

    @Override
    public void startOf(PoemType.StanzaType o) {

    }

    @Override
    public void endOf(PoemType.StanzaType o) {

    }

    @Override
    public void visit(DateType o) {

    }

    @Override
    public void visit(ImageType o) {

    }

    @Override
    public void startOf(PType o) {
        System.out.println("<p>");
        System.out.println("-----------------------------------------");;
        o.getContent().forEach(System.out::println);
        System.out.println("-----------------------------------------");;

    }

    @Override
    public void endOf(PType o) {
        System.out.println("+++++++++++++++++++++++++++");;

        System.out.println("</p>");

    }

    @Override
    public void startOf(SubTitle o) {

    }

    @Override
    public void endOf(SubTitle o) {

    }

    @Override
    public void startOf(CiteType o) {

    }

    @Override
    public void endOf(CiteType o) {

    }

    @Override
    public void startOf(FictionBook.Description o) {

    }

    @Override
    public void endOf(FictionBook.Description o) {

    }

    @Override
    public void startOf(TitleInfoType o) {

    }

    @Override
    public void endOf(TitleInfoType o) {

    }

    @Override
    public void startOf(FictionBook.Description.DocumentInfo o) {

    }

    @Override
    public void endOf(FictionBook.Description.DocumentInfo o) {

    }

    @Override
    public void visit(FictionBook.Description.PublishInfo o) {

    }

    @Override
    public void emptyLine() {
        System.out.println();
    }

    @Override
    public void unknownElement(Object o) {
        System.out.println(o);
    }
}
