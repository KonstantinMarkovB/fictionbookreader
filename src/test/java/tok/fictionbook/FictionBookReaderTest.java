package tok.fictionbook;

import org.junit.Test;
import tok.AbstractTest;
import tok.fictionbook.model.FictionBook;

public class FictionBookReaderTest extends AbstractTest {

    @Test
    public void test() {
        FictionBook fictionBook = read();
//        fictionBook.accept(new VisitorImpl());
        System.out.println(fictionBook.toString());

    }

    @Override
    protected String getFileName() {
        return "/sample1.fb2";
    }
}