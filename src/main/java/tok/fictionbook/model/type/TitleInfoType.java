
package tok.fictionbook.model.type;

import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "title-infoType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
        "genre",
        "author",
        "bookTitle",
        "annotation",
        "keywords",
        "date",
        "coverpage",
        "lang",
        "srcLang",
        "translator",
        "sequence"
})
@Data
public class TitleInfoType implements Visitable {

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
    @NotNull // 1..n
    protected List<TitleInfoType.Genre> genre = new ArrayList<Genre>();

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
    @NotNull // 1..n
    protected List<TitleInfoType.Author> author = new ArrayList<Author>();

    @XmlElement(name = "book-title", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
    @NotNull
    protected TextFieldType bookTitle;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected AnnotationType annotation;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected TextFieldType keywords;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected DateType date;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected TitleInfoType.Coverpage coverpage;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
    @NotNull
    protected String lang;

    @XmlElement(name = "src-lang", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected String srcLang;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<AuthorType> translator;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<SequenceType> sequence;

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
       if(annotation != null) annotation.accept(visitor);
        visitor.endOf(this);
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @Data
    @EqualsAndHashCode(callSuper = true)
    @SuperBuilder
    @AllArgsConstructor
    public static class Author extends AuthorType {
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"image"})
    @Data
    public static class Coverpage {
        @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
        @NotNull
        protected List<InlineImageType> image;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"value"})
    @Data
    @ToString(of = "value")
    public static class Genre {
        @XmlValue
        @NotNull
        protected GenreType value;

        @XmlAttribute(name = "match")
        protected BigInteger match = new BigInteger("100");
    }

}
