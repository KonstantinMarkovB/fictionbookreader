
package tok.fictionbook.model.type;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "docGenerationInstructionType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@XmlEnum
public enum DocGenerationInstructionType {

    @XmlEnumValue("require")
    REQUIRE("require"),
    @XmlEnumValue("allow")
    ALLOW("allow"),
    @XmlEnumValue("deny")
    DENY("deny");

    private final String value;

    DocGenerationInstructionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DocGenerationInstructionType fromValue(String v) {
        for (DocGenerationInstructionType c: DocGenerationInstructionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
