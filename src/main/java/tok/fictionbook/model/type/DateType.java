
package tok.fictionbook.model.type;

import lombok.Data;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dateType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {"value"})
@Data
public class DateType implements Visitable {

    @XmlValue
    protected String value;

    @XmlAttribute(name = "value1")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar value1;

    @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String lang;

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
