
package tok.fictionbook.model.type;

import lombok.Data;
import tok.fictionbook.model.element.PartShareInstruction;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "shareInstructionType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
    "partOrOutputDocumentClass"
})
@Data
public class ShareInstructionType {

    @XmlElements({
        @XmlElement(name = "part", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = PartShareInstruction.class),
        @XmlElement(name = "output-document-class", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = OutPutDocument.class)
    })
    protected List<Object> partOrOutputDocumentClass;

    @XmlAttribute(name = "mode", required = true)
    protected ShareModesType mode;

    @XmlAttribute(name = "include-all", required = true)
    protected DocGenerationInstructionType includeAll;

    @XmlAttribute(name = "price")
    protected Float price;

    @XmlAttribute(name = "currency")
    protected String currency;
}
