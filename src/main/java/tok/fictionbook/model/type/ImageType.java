
package tok.fictionbook.model.type;

import lombok.Data;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "imageType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@Data
public class ImageType implements Visitable {

    @XmlAttribute(name = "type", namespace = "http://www.w3.org/1999/xlink")
    protected String type = "simple";

    @XmlAttribute(name = "href", namespace = "http://www.w3.org/1999/xlink")
    protected String href;

    @XmlAttribute(name = "alt")
    protected String alt;

    @XmlAttribute(name = "title")
    protected String title;

    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
