
package tok.fictionbook.model.type;

import com.sun.istack.NotNull;
import lombok.Data;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bodyType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
    "image",
    "title",
    "epigraph",
    "section"
})
@Data
public class BodyType implements Visitable {

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected ImageType image;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected TitleType title;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<EpigraphType> epigraph = new LinkedList<>();

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
    @NotNull // 1..n
    protected List<SectionType> section = new LinkedList<>();

    @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String lang;

    @Override
    public void accept(Visitor visitor) {
        startAccept(visitor);
        if(title != null) title.accept(visitor);
        epigraph.forEach(it -> it.accept(visitor));
        section.forEach(it -> it.accept(visitor));
        endAccept(visitor);
    }

    protected void startAccept(Visitor visitor) {
        visitor.startOf(this);
    }

    protected void endAccept(Visitor visitor) {
        visitor.endOf(this);
    }

}
