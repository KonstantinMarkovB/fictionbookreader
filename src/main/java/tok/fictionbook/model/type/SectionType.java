
package tok.fictionbook.model.type;

import lombok.Data;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;
import tok.fictionbook.model.VisitorTool;
import tok.fictionbook.model.element.SubTitle;
import tok.fictionbook.model.element.*;
import tok.fictionbook.model.table.Table;
import tok.fictionbook.model.table.TableType;

import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sectionType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
    "title",
    "epigraph",
    "image",
    "annotation",
    "section",
    "p",
    "poem",
    "subtitle",
    "cite",
    "emptyLine",
    "table",
    "pOrImageOrPoem"
})
@Data
public class SectionType implements Visitable {

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected TitleType title;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<EpigraphType> epigraph = new LinkedList<>();

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected ImageType image;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected AnnotationType annotation;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<SectionType> section = new LinkedList<>();

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected PType p;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected PoemType poem;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected PType subtitle;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected CiteType cite;

    @XmlElement(name = "empty-line", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected EmptyLine emptyLine;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected TableType table;

    @XmlElementRefs({
        @XmlElementRef(name = "image", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Image.class, required = false),
        @XmlElementRef(name = "empty-line", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = EmptyLine.class, required = false),
        @XmlElementRef(name = "p", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = P.class, required = false),
        @XmlElementRef(name = "cite", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Cite.class, required = false),
        @XmlElementRef(name = "subtitle", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = SubTitle.class, required = false),
        @XmlElementRef(name = "poem", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Poem.class, required = false),
        @XmlElementRef(name = "table", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Table.class, required = false)
    })
    protected List<Object> pOrImageOrPoem = new LinkedList<>();

    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String lang;

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        if(title != null) title.accept(visitor);
        epigraph.forEach(it -> it.accept(visitor));
        if(image != null) image.accept(visitor);
        if(annotation != null) annotation.accept(visitor);
        section.forEach(it -> it.accept(visitor));
        if(p != null) p.accept(visitor);
        if(poem != null) poem.accept(visitor);
        if(subtitle != null) subtitle.accept(visitor);
        if(cite != null) cite.accept(visitor);
        if(emptyLine != null) emptyLine.accept(visitor);
        if(table != null) table.accept(visitor);
        VisitorTool.visitCollection(pOrImageOrPoem, visitor);

        visitor.endOf(this);
    }
}
