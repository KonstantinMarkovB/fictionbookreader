
package tok.fictionbook.model.type;

import lombok.Data;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;
import tok.fictionbook.model.VisitorTool;
import tok.fictionbook.model.element.EmptyLine;
import tok.fictionbook.model.element.P;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "titleType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
    "pOrEmptyLine"
})
@Data
public class TitleType implements Visitable {

    @XmlElements({
        @XmlElement(name = "p", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = P.class),
        @XmlElement(name = "empty-line", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = EmptyLine.class)
    })
    protected List<Object> pOrEmptyLine;

    @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String lang;

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        VisitorTool.visitCollection(pOrEmptyLine, visitor);
        visitor.endOf(this);
    }
}
