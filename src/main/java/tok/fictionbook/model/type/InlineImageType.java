
package tok.fictionbook.model.type;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inlineImageType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@Data
public class InlineImageType {

    @XmlAttribute(name = "type", namespace = "http://www.w3.org/1999/xlink")
    protected String type = "simple";

    @XmlAttribute(name = "href", namespace = "http://www.w3.org/1999/xlink")
    protected String href;

    @XmlAttribute(name = "alt")
    protected String alt;
}
