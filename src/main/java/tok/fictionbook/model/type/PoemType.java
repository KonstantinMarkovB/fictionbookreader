
package tok.fictionbook.model.type;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;
import tok.fictionbook.model.VisitorTool;
import tok.fictionbook.model.element.SubTitle;

import java.util.List;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "poemType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
        "title",
        "epigraph",
        "subtitleOrStanza",
        "textAuthor",
        "date"
})
@Data
public class PoemType implements Visitable {

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected TitleType title;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<EpigraphType> epigraph;

    @XmlElements({
            @XmlElement(name = "subtitle", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = SubTitle.class),
            @XmlElement(name = "stanza", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = PoemType.Stanza.class)
    })
    protected List<Object> subtitleOrStanza;

    @XmlElement(name = "text-author", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<PType> textAuthor;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected DateType date;

    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String lang;

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        title.accept(visitor);
        epigraph.forEach(it -> it.accept(visitor));
        VisitorTool.visitCollection(subtitleOrStanza, visitor);
        textAuthor.forEach(it -> it.accept(visitor));
        date.accept(visitor);
        visitor.endOf(this);
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"title", "subtitle", "v"})
    @Data
    public static class StanzaType implements Visitable {

        @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
        protected TitleType title;

        @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
        protected PType subtitle;

        @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
        protected List<PType> v;

        @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "language")
        protected String lang;

        @Override
        public void accept(Visitor visitor) {
            visitor.startOf(this);
            title.accept(visitor);
            subtitle.accept(visitor);
            v.forEach(it -> it.accept(visitor));
            visitor.endOf(this);
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlRootElement(name = "")
    @Data
    @EqualsAndHashCode(callSuper = true)
    public static class Stanza extends StanzaType {
    }


}
