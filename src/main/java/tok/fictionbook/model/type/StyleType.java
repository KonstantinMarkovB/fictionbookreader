
package tok.fictionbook.model.type;

import lombok.Data;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;
import tok.fictionbook.model.VisitorTool;
import tok.fictionbook.model.element.Sub;
import tok.fictionbook.model.element.Sup;
import tok.fictionbook.model.element.*;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "styleType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
        "content"
})
@Data
public class StyleType implements Visitable {

    // Also can contains String values
    @XmlElementRefs({
            @XmlElementRef(name = "strong", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Strong.class, required = false),
            @XmlElementRef(name = "image", type = Image.class, required = false),
            @XmlElementRef(name = "a", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = A.class, required = false),
            @XmlElementRef(name = "emphasis", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Emphasis.class, required = false),
            @XmlElementRef(name = "style", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Style.class, required = false),
            @XmlElementRef(name = "strikethrough", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Strikethrough.class, required = false),
            @XmlElementRef(name = "sub", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Sub.class, required = false),
            @XmlElementRef(name = "sup", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Sup.class, required = false),
            @XmlElementRef(name = "code", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Code.class, required = false),
    })
    @XmlMixed
    protected List<Object> content;

    @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String lang;

    @Override
    public void accept(Visitor visitor) {
        VisitorTool.visitCollection(content, visitor);
    }
}
