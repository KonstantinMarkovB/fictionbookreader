
package tok.fictionbook.model.type;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "vAlignType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@XmlEnum
public enum VAlignType {

    @XmlEnumValue("top")
    TOP("top"),
    @XmlEnumValue("middle")
    MIDDLE("middle"),
    @XmlEnumValue("bottom")
    BOTTOM("bottom");
    private final String value;

    VAlignType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VAlignType fromValue(String v) {
        for (VAlignType c: VAlignType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
