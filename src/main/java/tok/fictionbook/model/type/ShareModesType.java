
package tok.fictionbook.model.type;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "shareModesType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@XmlEnum
public enum ShareModesType {

    @XmlEnumValue("free")
    FREE("free"),
    @XmlEnumValue("paid")
    PAID("paid");
    private final String value;

    ShareModesType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ShareModesType fromValue(String v) {
        for (ShareModesType c: ShareModesType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
