
package tok.fictionbook.model.type;

import lombok.Data;
import tok.fictionbook.model.element.Sub;
import tok.fictionbook.model.element.Sup;
import tok.fictionbook.model.element.*;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "styleLinkType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
    "content"
})
@Data
public class StyleLinkType {

    @XmlElementRefs({
        @XmlElementRef(name = "code", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Code.class, required = false),
        @XmlElementRef(name = "emphasis", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Emphasis.class, required = false),
        @XmlElementRef(name = "strikethrough", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Strikethrough.class, required = false),
        @XmlElementRef(name = "image", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Image.class, required = false),
        @XmlElementRef(name = "sub", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Sub.class, required = false),
        @XmlElementRef(name = "strong", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Strong.class, required = false),
        @XmlElementRef(name = "style", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Style.class, required = false),
        @XmlElementRef(name = "sup", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Sup.class, required = false)
    })
    @XmlMixed
    protected List<Serializable> content;
}
