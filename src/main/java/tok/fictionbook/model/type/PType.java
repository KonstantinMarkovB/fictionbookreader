
package tok.fictionbook.model.type;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@Data
@EqualsAndHashCode(callSuper = true)
public class PType extends StyleType implements Visitable {

    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    @XmlAttribute(name = "style")
    protected String style;

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        super.accept(visitor);
        visitor.endOf(this);
    }
}
