
package tok.fictionbook.model.type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import static tok.fictionbook.model.Constants.NAMESPACE;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "authorType", namespace = NAMESPACE)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class AuthorType {
    private static final String FIRST_NAME = "first-name";

    @XmlElement(name = "first-name", namespace = NAMESPACE)
    private String firstName;

    @XmlElement(name = "last-name", namespace = NAMESPACE)
    private String lastName;

    @XmlElement(name = "middle-name", namespace = NAMESPACE)
    private String middleName;

    @XmlElement(name = "nickname", namespace = NAMESPACE)
    private String nickname;

    @XmlElement(name = "email", namespace = NAMESPACE)
    private String email;

    @XmlElement(name = "home-page", namespace = NAMESPACE)
    private String homePage;

    @XmlElement(name = "id", namespace = NAMESPACE)
    private String id;

}
