
package tok.fictionbook.model.type;

import lombok.Data;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;
import tok.fictionbook.model.VisitorTool;
import tok.fictionbook.model.element.P;
import tok.fictionbook.model.element.Poem;
import tok.fictionbook.model.element.Cite;
import tok.fictionbook.model.element.EmptyLine;

import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "epigraphType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
    "pOrPoemOrCite",
    "textAuthor"
})
@Data
public class EpigraphType implements Visitable {

    @XmlElements({
        @XmlElement(name = "p", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = P.class),
        @XmlElement(name = "poem", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Poem.class),
        @XmlElement(name = "cite", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Cite.class),
        @XmlElement(name = "empty-line", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = EmptyLine.class)
    })
    protected List<Object> pOrPoemOrCite = new LinkedList<>();

    @XmlElement(name = "text-author", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<PType> textAuthor = new LinkedList<>();

    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        VisitorTool.visitCollection(pOrPoemOrCite, visitor);
        textAuthor.forEach(it -> it.accept(visitor));
        visitor.endOf(this);
    }
}
