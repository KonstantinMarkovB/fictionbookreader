
package tok.fictionbook.model.type;

import lombok.Data;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "outPutDocumentType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
    "part"
})
@Data
public class OutPutDocumentType {

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<PartShareInstructionType> part;

    @XmlAttribute(name = "name", required = true)
    protected String name;

    @XmlAttribute(name = "create")
    protected DocGenerationInstructionType create;

    @XmlAttribute(name = "price")
    protected Float price;

}
