
package tok.fictionbook.model.type;

import lombok.Data;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;
import tok.fictionbook.model.VisitorTool;
import tok.fictionbook.model.element.EmptyLine;
import tok.fictionbook.model.element.P;
import tok.fictionbook.model.element.Poem;
import tok.fictionbook.model.element.SubTitle;
import tok.fictionbook.model.table.Table;

import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "citeType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
    "pOrPoemOrEmptyLine",
    "textAuthor"
})
@Data
public class CiteType implements Visitable {

    @XmlElementRefs({
        @XmlElementRef(name = "table", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Table.class, required = false),
        @XmlElementRef(name = "p", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = P.class, required = false),
        @XmlElementRef(name = "poem", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Poem.class, required = false),
        @XmlElementRef(name = "empty-line", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = EmptyLine.class, required = false),
        @XmlElementRef(name = "subtitle", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = SubTitle.class, required = false)
    })
    protected List<Object> pOrPoemOrEmptyLine = new LinkedList<>();

    @XmlElement(name = "text-author", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<PType> textAuthor = new LinkedList<>();

    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String lang;

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        VisitorTool.visitCollection(pOrPoemOrEmptyLine, visitor);
        textAuthor.forEach(it -> it.accept(visitor));
        visitor.endOf(this);
    }
}
