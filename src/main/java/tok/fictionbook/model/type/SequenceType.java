
package tok.fictionbook.model.type;

import lombok.Data;

import java.math.BigInteger;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sequenceType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
    "sequence"
})
@Data
public class SequenceType {

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<SequenceType> sequence;

    @XmlAttribute(name = "name", required = true)
    protected String name;

    @XmlAttribute(name = "number")
    protected BigInteger number;

    @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String lang;
}
