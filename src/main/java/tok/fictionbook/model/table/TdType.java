
package tok.fictionbook.model.table;

import tok.fictionbook.model.type.StyleType;
import tok.fictionbook.model.type.VAlignType;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tdType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
public class TdType extends StyleType {

    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    @XmlAttribute(name = "style")
    protected String style;

    @XmlAttribute(name = "colspan")
    protected BigInteger colspan;

    @XmlAttribute(name = "rowspan")
    protected BigInteger rowspan;

    @XmlAttribute(name = "align")
    protected AlignType align = AlignType.LEFT;

    @XmlAttribute(name = "valign")
    protected VAlignType valign = VAlignType.TOP;
}
