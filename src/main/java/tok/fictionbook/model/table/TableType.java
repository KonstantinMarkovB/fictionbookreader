
package tok.fictionbook.model.table;

import lombok.Data;
import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;
import tok.fictionbook.model.VisitorTool;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tableType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", propOrder = {
        "tr"
})
@Data
public class TableType implements Visitable {

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
    protected List<TableType.Tr> tr;

    @XmlAttribute(name = "style")
    protected String style;

    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        visitor.endOf(this);
        VisitorTool.visitCollection(tr, visitor);
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"thOrTd"})
    @Data
    public static class Tr implements Visitable {

        @XmlElementRefs({
                @XmlElementRef(name = "th", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Th.class, required = false),
                @XmlElementRef(name = "td", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", type = Td.class, required = false)
        })
        protected List<Object> thOrTd;

        @XmlAttribute(name = "align")
        protected AlignType align;

        @Override
        public void accept(Visitor visitor) {
            visitor.startOf(this);
            VisitorTool.visitCollection(thOrTd, visitor);
            visitor.endOf(this);
        }
    }

}
