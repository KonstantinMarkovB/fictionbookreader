
package tok.fictionbook.model.table;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "table", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
public class Table extends TableType {
}
