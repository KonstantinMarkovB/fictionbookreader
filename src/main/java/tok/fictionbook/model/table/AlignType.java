
package tok.fictionbook.model.table;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "alignType", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@XmlEnum
public enum AlignType {

    @XmlEnumValue("left")
    LEFT("left"),
    @XmlEnumValue("right")
    RIGHT("right"),
    @XmlEnumValue("center")
    CENTER("center");

    private final String value;

    AlignType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AlignType fromValue(String v) {
        for (AlignType c: AlignType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
