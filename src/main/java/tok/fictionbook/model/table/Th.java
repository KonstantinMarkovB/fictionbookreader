
package tok.fictionbook.model.table;

import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "th", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
public class Th extends TdType implements Visitable {

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        super.accept(visitor);
        visitor.endOf(this);
    }

}
