
package tok.fictionbook.model.table;

import tok.fictionbook.model.Visitor;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "td", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
public class Td extends TdType {
    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        super.accept(visitor);
        visitor.endOf(this);
    }
}
