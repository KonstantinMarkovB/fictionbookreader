package tok.fictionbook.model;

import tok.fictionbook.model.element.SubTitle;
import tok.fictionbook.model.table.TableType;
import tok.fictionbook.model.table.Td;
import tok.fictionbook.model.table.Th;
import tok.fictionbook.model.type.*;

public interface Visitor {
    void startOf(FictionBook o);
    void endOf(FictionBook o);
    void startOf(BodyType o);
    void endOf(BodyType o);
    void startOf(NotesBodyType o);
    void endOf(NotesBodyType o);
    void startOf(EpigraphType o);
    void endOf(EpigraphType o);
    void startOf(SectionType o);
    void endOf(SectionType o);
    void startOf(TableType o);
    void endOf(TableType o);
    void startOf(TitleType o);
    void endOf(TitleType o);
    void startOf(TableType.Tr o);
    void endOf(TableType.Tr o);
    void startOf(Th o);
    void endOf(Th o);
    void startOf(Td o);
    void endOf(Td o);
    void startOf(AnnotationType o);
    void endOf(AnnotationType o);
    void startOf(PoemType o);
    void endOf(PoemType o);
    void startOf(PoemType.StanzaType o);
    void endOf(PoemType.StanzaType o);
    void visit(DateType o);
    void visit(ImageType o);

    void startOf(PType o);
    void endOf(PType o);
    void startOf(SubTitle o);
    void endOf(SubTitle o);
    void startOf(CiteType o);
    void endOf(CiteType o);
    void startOf(FictionBook.Description o);
    void endOf(FictionBook.Description o);
    void startOf(TitleInfoType o);
    void endOf(TitleInfoType o);
    void startOf(FictionBook.Description.DocumentInfo o);
    void endOf(FictionBook.Description.DocumentInfo o);
    void visit(FictionBook.Description.PublishInfo o);
    void emptyLine();

    void unknownElement(Object o);
}
