package tok.fictionbook.model;

import tok.fictionbook.model.table.TableType;
import tok.fictionbook.model.table.Td;
import tok.fictionbook.model.table.Th;

import java.util.List;

public class VisitorTool {
   public static void visitCollection(List<?> objects, Visitor visitor) {
       objects.forEach(it -> {
           if(it instanceof Visitable) {
               ((Visitable) it).accept(visitor);
           } else {
               visitor.unknownElement(it);
           }
       });
   }
}
