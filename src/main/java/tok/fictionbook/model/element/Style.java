
package tok.fictionbook.model.element;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tok.fictionbook.model.type.StyleType;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "style", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@Data
@EqualsAndHashCode(callSuper = true)
public class Style extends StyleType {

    @XmlAttribute(name = "name")
    protected String name;

    @XmlAttribute(name = "lang")
    protected String lang;
}
