
package tok.fictionbook.model.element;

import tok.fictionbook.model.Visitor;
import tok.fictionbook.model.type.StyleType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "subtitle", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
public class SubTitle extends StyleType {
    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        super.accept(visitor);
        visitor.endOf(this);
    }
}
