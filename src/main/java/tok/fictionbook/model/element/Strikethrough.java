
package tok.fictionbook.model.element;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tok.fictionbook.model.type.StyleType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "strikethrough", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@Data
@EqualsAndHashCode(callSuper = true)
public class Strikethrough extends StyleType { }
