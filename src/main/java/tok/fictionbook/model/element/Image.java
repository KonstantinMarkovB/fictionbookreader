
package tok.fictionbook.model.element;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tok.fictionbook.model.type.ImageType;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "image", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@Data
@EqualsAndHashCode(callSuper = true)
public class Image extends ImageType {
}
