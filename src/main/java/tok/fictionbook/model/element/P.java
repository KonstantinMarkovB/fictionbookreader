
package tok.fictionbook.model.element;

import tok.fictionbook.model.type.PType;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "p", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
public class P extends PType { }
