
package tok.fictionbook.model.element;

import tok.fictionbook.model.Visitable;
import tok.fictionbook.model.Visitor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "empty-line", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
public class EmptyLine implements Visitable {
    @Override
    public void accept(Visitor visitor) {
        visitor.emptyLine();
    }
}
