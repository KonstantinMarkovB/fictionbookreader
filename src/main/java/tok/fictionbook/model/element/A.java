
package tok.fictionbook.model.element;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tok.fictionbook.model.type.StyleType;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "a", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@Data
@EqualsAndHashCode(callSuper = true)
public class A extends StyleType {
    @XmlAttribute(name = "type", namespace = "http://www.w3.org/1999/xlink")
    protected String xtype;

    @XmlAttribute(name = "href", namespace = "http://www.w3.org/1999/xlink", required = true)
    protected String href;

    @XmlAttribute(name = "type")
    protected String type;
}
