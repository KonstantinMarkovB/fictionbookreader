
package tok.fictionbook.model.element;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tok.fictionbook.model.type.PoemType;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "poem", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@Data
@EqualsAndHashCode(callSuper = true)
public class Poem  extends PoemType { }
