
package tok.fictionbook.model;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import tok.fictionbook.model.type.*;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "stylesheet",
        "description",
        "body",
        "notesBody",
        "binary"
})
@XmlRootElement(name = "FictionBook", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
@Data
public class FictionBook implements Visitable {

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<FictionBook.Stylesheet> stylesheet;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
    protected FictionBook.Description description;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
    protected BodyType body;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected NotesBodyType notesBody;

    @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
    protected List<FictionBook.Binary> binary;

    @Override
    public void accept(Visitor visitor) {
        visitor.startOf(this);
        if(description != null) description.accept(visitor);
        body.accept(visitor);
        if(notesBody != null) notesBody.accept(visitor);
        visitor.endOf(this);
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"value"})
    @Getter
    @Setter
    public static class Binary {

        @XmlValue
        protected byte[] value;

        @XmlAttribute(name = "content-type", required = true)
        protected String contentType;

        @XmlAttribute(name = "id", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        @XmlSchemaType(name = "ID")
        protected String id;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "titleInfo",
            "srcTitleInfo",
            "documentInfo",
            "publishInfo",
            "customInfo",
            "output"
    })
    @Data
    public static class Description implements Visitable {

        @XmlElement(name = "title-info", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
        @NotNull
        protected TitleInfoType titleInfo;

        @XmlElement(name = "src-title-info", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
        protected TitleInfoType srcTitleInfo;

        @XmlElement(name = "document-info", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
        protected FictionBook.Description.DocumentInfo documentInfo;

        @XmlElement(name = "publish-info", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
        protected FictionBook.Description.PublishInfo publishInfo;

        @XmlElement(name = "custom-info", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
        protected List<FictionBook.Description.CustomInfo> customInfo;

        @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
        protected List<ShareInstructionType> output;

        @Override
        public void accept(Visitor visitor) {
            visitor.startOf(this);
            titleInfo.accept(visitor);
            titleInfo.accept(visitor);
            documentInfo.accept(visitor);
            publishInfo.accept(visitor);
            visitor.endOf(this);
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        @Data
        @EqualsAndHashCode(callSuper = true)
        public static class CustomInfo extends TextFieldType {
            @XmlAttribute(name = "info-type", required = true)
            protected String infoType;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "author",
                "programUsed",
                "date",
                "srcUrl",
                "srcOcr",
                "id",
                "version",
                "history",
                "publisher"
        })
        @Data
        public static class DocumentInfo implements Visitable {

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
            protected List<AuthorType> author;

            @XmlElement(name = "program-used", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected TextFieldType programUsed;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
            protected DateType date;

            @XmlElement(name = "src-url", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected List<String> srcUrl;

            @XmlElement(name = "src-ocr", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected TextFieldType srcOcr;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0", required = true)
            @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
            @XmlSchemaType(name = "token")
            protected String id;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected float version;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected AnnotationType history;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected List<AuthorType> publisher;

            @Override
            public void accept(Visitor visitor) {
                visitor.startOf(this);
                if(history != null) history.accept(visitor);
                visitor.endOf(this);
            }
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "bookName",
                "publisher",
                "city",
                "year",
                "isbn",
                "sequence"
        })
        @Data
        public static class PublishInfo implements Visitable{

            @XmlElement(name = "book-name", namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected TextFieldType bookName;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected TextFieldType publisher;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected TextFieldType city;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            @XmlSchemaType(name = "gYear")
            protected XMLGregorianCalendar year;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected TextFieldType isbn;

            @XmlElement(namespace = "http://www.gribuser.ru/xml/fictionbook/2.0")
            protected List<SequenceType> sequence;

            @Override
            public void accept(Visitor visitor) {
                visitor.visit(this);
            }
        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "value"
    })
    @Data
    public static class Stylesheet {

        @XmlValue
        protected String value;

        @XmlAttribute(name = "type", required = true)
        protected String type;
    }

}
