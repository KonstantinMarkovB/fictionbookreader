package tok.fictionbook.model;

public interface Visitable {
    void accept(Visitor visitor);
}
