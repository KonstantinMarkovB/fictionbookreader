package tok.fictionbook;

import tok.fictionbook.model.FictionBook;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class FictionBookReader {

    public FictionBook read(File file) throws FileNotFoundException, JAXBException, XMLStreamException {
        return read(new FileInputStream(file));
    }

    public FictionBook read(InputStream inputStream) throws XMLStreamException, JAXBException {
        XMLInputFactory xif = XMLInputFactory.newFactory();
        XMLStreamReader xsr = xif.createXMLStreamReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        JAXBContext context = JAXBContext.newInstance(FictionBook.class);
        return (FictionBook) context.createUnmarshaller().unmarshal(xsr);
    }

}
